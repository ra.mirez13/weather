

import Foundation
import CoreData


extension WeatherLocation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherLocation> {
        return NSFetchRequest<WeatherLocation>(entityName: "WeatherLocation")
    }

    @NSManaged public var cityName: String?
    @NSManaged public var date: String?
    @NSManaged public var weatherType: String?
    @NSManaged public var weatherMinTemp: Double
    @NSManaged public var weatherMaxTemp: Double
    @NSManaged public var currentTemp: Double
    @NSManaged public var long: Double
    @NSManaged public var lat: Double

}
