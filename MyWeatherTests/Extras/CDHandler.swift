
import CoreData
import Foundation
import UIKit
var objects : [WeatherLocation] = []
class CDHandler: NSObject {
    //func возвращает  контекст обьекта
    private class func getContext()  -> NSManagedObjectContext{
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    class func saveObject(weatherType:String,weatherMinTemp:Double,weatherMaxTemp:Double,date:String,currentTemp:Double,cityName:String ) -> Bool {
        let contex = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "MyWeatherTests", in: contex)
        //создаем обьект
        let managedObject = NSManagedObject(entity: entity!, insertInto: contex)
        managedObject.setValue(weatherType, forKey: "weatherMinTemp")
        managedObject.setValue(weatherMinTemp, forKey: "weatherMinTemp")
        managedObject.setValue(weatherMaxTemp, forKey: "weatherMaxTemp")
        managedObject.setValue(date, forKey: "date")
        managedObject.setValue(currentTemp, forKey: "currentTemp")
        managedObject.setValue(cityName, forKey: "cityName")
        do{
            try contex.save()
             print("sssssssssssssssssssssss Core Data save complit")
            return true
        }catch{
            return false
        }
    }
    
    //получаем обьект с нашей базы
    
    class func fetchObject()  -> [WeatherLocation]?{
        let context = getContext()
        var weatherLocation:[WeatherLocation]? = nil
        do{
            weatherLocation = try context.fetch(WeatherLocation.fetchRequest())
            return weatherLocation
        }catch{
            return weatherLocation
        }
        
    
    
    }
  class  func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            objects = try context.fetch(WeatherLocation.fetchRequest())
             print("objeсcts\(objects)")
            
        }
        catch let error as NSError {
           print("Error, \(error.localizedDescription)")
        }
    }
    
    ///delete all the data in core data
    class func cleanCoreData() {
        
        let fetchRequest:NSFetchRequest<WeatherLocation> = WeatherLocation.fetchRequest()
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        
        do {
            print("deleting all contents")
            try getContext().execute(deleteRequest)
        }catch {
            print(error.localizedDescription)
        }
        
    
}
}
