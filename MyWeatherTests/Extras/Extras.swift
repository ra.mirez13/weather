
import UIKit
import Foundation
import CoreData


let API_URL = "https://api.openweathermap.org/data/2.5/weather?APPID=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru&units=weather&lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)"
let FORECAST_API_URL = "https://api.openweathermap.org/data/2.5/forecast/daily?cnt=8&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)"

let API_URL_KIEV = "https://api.openweathermap.org/data/2.5/weather?q=Kiev,ua&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru"
let FORECAST_URL_KIEV = "https://api.openweathermap.org/data/2.5/forecast/daily?q=Kiev&units=weather&cnt=7&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru"

let API_URL_SEARCH = "https://api.openweathermap.org/data/2.5/weather?q=\(NameCity.sharedInstance.city!)&units=weather&cnt=7&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru"
let FORECAST_URL_SEARCH = "https://api.openweathermap.org/data/2.5/forecast/daily?q=\(NameCity.sharedInstance.city!)&units=weather&cnt=7&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru"



//обработчик

typealias DownloadComplete = () -> ()

