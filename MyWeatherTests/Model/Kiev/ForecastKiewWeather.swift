

import Foundation
import Alamofire
class ForecastKievWeather{
    private var _date:String!
    private var _temp:Double!
    
    var data:String{
        if _date == nil{
            _date = ""
        }
        return _date
    }
    var temp:Double{
        if _temp == nil{
            _temp = 0.0
        }
        return _temp
    }
    
    init(weatherDict:Dictionary<String, AnyObject>) {
        if let temp = weatherDict["temp"] as? Dictionary<String, AnyObject>{
            if let dayTemp = temp["day"] as? Double{
//let rowValue  = (dayTemp - 273.15).rounded(toPlaces: 0)
                let rowValue  = dayTemp 
                self._temp = rowValue
            }
        }
        if let date = weatherDict["dt"] as? Double{
            let rawDate =  Date(timeIntervalSince1970: date)
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ru_RU")
            dateFormatter.dateStyle = .medium
            self._date = "\(rawDate.dayOfTheWeek())"
            
        }
    }
    
    
}
