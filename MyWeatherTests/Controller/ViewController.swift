
import UIKit
import CoreLocation
import Alamofire
import CoreData

class ViewController: UIViewController,CLLocationManagerDelegate {
    //MARK:Outlets
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentCityTemp: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var specialBG: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var currentMinCityTemp: UILabel!
    @IBOutlet weak var currentMaxCityTemp: UILabel!
    
    
    //MARK:Constants
    let locationManager  = CLLocationManager()
    
    
    
    //MARK:Variables
    var currentWeather:CurrentWeather = CurrentWeather()
    var currentLocation:CLLocation!
    var forecastWeather:ForecastWeather!
    var forecastArray = [ForecastWeather]()
    var weather = [WeatherLocation]()
    var weatherData = WeatherLocation()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callDelegates()
        currentWeather = CurrentWeather()
        applyEffect()
        downloadForecastWeather{
            print("data Downloaded")
        }
        
    }
       
        override func viewDidAppear(_ animated: Bool) {
            currentWeather.downloadCurrentWeather {
            self.updateUI()
            }
        }
        
        private func callDelegates(){
            tableView.delegate = self
            tableView.dataSource = self
        }
        
        private func applyEffect(){
           // specialEffect(view: specialBG, intensity: 45)
            specialEffect(view: weatherImage, intensity: -15)
        }
        
        
        //MARK: Move Background
        private  func specialEffect(view:UIView, intensity:Double)  {
            let horizontalMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
            horizontalMotion.minimumRelativeValue = -intensity
            horizontalMotion.maximumRelativeValue = intensity
            
            let verticalMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
            verticalMotion.minimumRelativeValue = -intensity
            verticalMotion.maximumRelativeValue = intensity
            
            //add move
            let movement = UIMotionEffectGroup()
            movement.motionEffects = [horizontalMotion,verticalMotion]
            view.addMotionEffect(movement)
        }
        
        //updateInterface
        
        private func updateUI(){
            
            let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext
            let fetchRequest = WeatherLocation.fetchRequest() as NSFetchRequest<WeatherLocation>
            do {
                weather = try context!.fetch(fetchRequest)
             } catch let error {
                print("\(error)")
            }
            for w in weather{
                cityName.text = w.cityName
               currentCityTemp.text = "\(Int(w.currentTemp))"
                weatherType.text = w.weatherType
                currentDate.text = w.date
               
              
            }
            
//            cityName.text = currentWeather.cityName
//            currentCityTemp.text = "\(Int(currentWeather.currentTemp))"
//            //        currentMaxCityTemp.text = "\(Int(currentWeather.weatherMaxTemp))"
//            //        currentMinCityTemp.text = "\(Int(currentWeather.weatherMinTemp))"
//            weatherType.text = currentWeather.weatherType
//            currentDate.text = currentWeather.data
        }
        
        private func downloadForecastWeather(completed:@escaping DownloadComplete){
            Alamofire.request(FORECAST_API_URL).responseJSON { (response) in
                let result  = response.result
                //создаем словарь который хранит все данные
                if let dictionary  = result.value as? Dictionary<String, AnyObject>{
                    //храним все обьекты в массиве list в Словаре(Dictionary)
                    if let list  =  dictionary["list"] as? [Dictionary<String, AnyObject>]{
                        for item in list{
                            let forecast = ForecastWeather(weatherDict: item)
                            self.forecastArray.append(forecast)
                        }
                        self.forecastArray.remove(at: 0)
                        self.tableView.reloadData()
                    }
                }
                completed()
            }
        }
    }
    
    
    extension ViewController:UITableViewDelegate,UITableViewDataSource{
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return forecastArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! ForecastTableViewCell
            cell.configureCell(forecastData: forecastArray[indexPath.row])
            
            return cell
        }
        
        
}
