
import UIKit
import Alamofire

class KievViewController: UIViewController {
    //MARK:Outlets
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentCityTemp: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var specialBG: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:Constants
    
    
    //MARK:Variables
    var currentKievWeather:KievWeather = KievWeather()
    var forecastWeather:ForecastWeather!
    var forecastArray = [ForecastWeather]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyEffect()
        currentKievWeather = KievWeather()
        callDelegates()
        downloadForecastWeather{
            print("data Downloaded")
        }
      
    }
  
    private func callDelegates(){
        tableView.delegate = self
        tableView.dataSource = self
    }
  
  
    
    override func viewDidAppear(_ animated: Bool) {
        currentKievWeather.downloadKiewWeather {
            self.updateUI()
            print("data Downloaded currentKievWeather")
        }
        
       
        
    }
   
    private func applyEffect(){
        //specialEffect(view: specialBG, intensity: 45)
        specialEffect(view: weatherImage, intensity: -15)
    }
    
    //MARK: Move Background
    private  func specialEffect(view:UIView, intensity:Double)  {
        let horizontalMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontalMotion.minimumRelativeValue = -intensity
        horizontalMotion.maximumRelativeValue = intensity
        let verticalMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        verticalMotion.minimumRelativeValue = -intensity
        verticalMotion.maximumRelativeValue = intensity
        
        //add move
        let movement = UIMotionEffectGroup()
        movement.motionEffects = [horizontalMotion,verticalMotion]
        view.addMotionEffect(movement)
    }
    
    //updateInterface
    
    private func updateUI(){
        cityName.text = currentKievWeather.cityName
        currentCityTemp.text = "\(Int(currentKievWeather.currentTemp))"
        weatherType.text = currentKievWeather.weatherType
        currentDate.text = currentKievWeather.data
    }
    
    private func downloadForecastWeather(completed:@escaping DownloadComplete){
        Alamofire.request(FORECAST_URL_KIEV).responseJSON { (response) in
            let result  = response.result
            //создаем словарь который хранит все данные
            if let dictionary  = result.value as? Dictionary<String, AnyObject>{
                //храним все обьекты в массиве list в Словаре(Dictionary)
                if let list  =  dictionary["list"] as? [Dictionary<String, AnyObject>]{
                    for item in list{
                        let forecast = ForecastWeather(weatherDict: item)
                        self.forecastArray.append(forecast)
                    }
                    self.forecastArray.remove(at: 0)
                    self.tableView.reloadData()
                }
            }
            completed()
        }
    }
}


extension KievViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! ForecastTableViewCell
        cell.configureCell(forecastData: forecastArray[indexPath.row])
        
        return cell
    }
    
    
}
