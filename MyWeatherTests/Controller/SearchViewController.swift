
import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation
import CoreData

class SearchViewController: UIViewController,UISearchBarDelegate,UISearchControllerDelegate,CLLocationManagerDelegate {
    //MARK:Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentCityTemp: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var specialBG: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:Constants
    let locationManager  = CLLocationManager()
    
    //MARK:Variables
    var currentLocation:CLLocation!
    var currentWeather:SearchWeather = SearchWeather()
    var searchController :UISearchController!
    var forecastArray = [ForecastWeather]()
    var text = ""
    var city:String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        callDelegates()
        currentWeather = SearchWeather()
        searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        //self.searchBar.becomeFirstResponder()
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        locationManager.requestWhenInUseAuthorization()
        locationAuthCheck()
       
    }
    func callDelegates(){
        searchBar.delegate = self
        tableView.delegate = self
        // searchController.delegate = self
    }
    
    
    //MARK: locationManager
    func locationAuthCheck() {
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            Location.sharedInstance.latitude = location.coordinate.latitude
            Location.sharedInstance.longitude = location.coordinate.longitude
            
            if let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext {
                let weatherData = WeatherLocation(context: context)
                weatherData.lat = location.coordinate.latitude
                weatherData.long = location.coordinate.longitude
                do {
                    try context.save()
                } catch let error {
                    print("\(error)")
                }
            }
        }
    }
   
    //MARK: SEARCH BAR
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""
        {
            text = searchText
            
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NameCity.sharedInstance.city = text
        let url = URL(string: "https://api.openweathermap.org/data/2.5/weather")!
        let APIKey = "7c609f73c5df2dff2f32e3e3cc33cd23"
        let params: Parameters = [
            "units":"weather",
            "cnt":"7",
            "q":self.text,
            "lang":"ru",
            "appid":APIKey
        ]
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default).responseJSON { (response) in
            let result = response.result
            let json = JSON(result.value!)
            let message: String = json["message"].stringValue
            if let cod:String = json["cod"].stringValue{
                if cod == "404"{
                    print(cod)
                    let alert = UIAlertController(title: message, message: "", preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                   // show the alert
                    self.present(alert, animated: true, completion: nil)
              }else{
                    self.cityName.text = json["name"].stringValue
                    let tempDate = json["dt"].double
                    let convertedDate =  Date(timeIntervalSince1970:  tempDate!)
                    let dateFormatter = DateFormatter()
                    dateFormatter.locale = Locale(identifier: "ru_RU")
                    dateFormatter.dateStyle = .medium
                    dateFormatter.timeStyle = .none
                    let currentDate = dateFormatter.string(from: convertedDate)
                    self.currentDate.text = "\(currentDate)"
                    self.weatherType.text = json["weather"][0]["description"].stringValue
                    let downloadetTemp = json["main"]["temp"].double
                    self.currentCityTemp.text = (String(Int(downloadetTemp! - 273.15 )))
                    let img = json["weather"][0]["icon"].stringValue
                    self.weatherImage.image = UIImage(named: img)
                    
                }
                self.downloadForecastWeather {
                    print("load data")
                }
            }
            
            
            
        }
                self.searchBar.endEditing(true)
    }
    private func downloadForecastWeather(completed:@escaping DownloadComplete){
        Alamofire.request("https://api.openweathermap.org/data/2.5/forecast/daily?q=\(self.text)&units=weather&cnt=7&appid=7c609f73c5df2dff2f32e3e3cc33cd23&lang=ru").responseJSON { (response) in
            print(response.request)
            
            let result  = response.result
            //создаем словарь который хранит все данные
            if let dictionary  = result.value as? Dictionary<String, AnyObject>{
                //храним все обьекты в массиве list в Словаре(Dictionary)
                if let list  =  dictionary["list"] as? [Dictionary<String, AnyObject>]{
                    for item in list{
                        let forecast = ForecastWeather(weatherDict: item)
                        self.forecastArray.append(forecast)
                    }
                    self.forecastArray.remove(at: 0)
                    self.tableView.reloadData()
                }
            }
        }
    }

}

extension SearchViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastCell") as! ForecastTableViewCell
      //  cell.configureCell(forecastData: forecastArray[indexPath.row])
        cell.configureCell(forecastData: forecastArray[indexPath.row])
        return cell
}





}


